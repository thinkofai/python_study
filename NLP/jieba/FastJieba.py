# 导入 jieba
import jieba
import jieba.posseg as pseg #词性标注
import jieba.analyse as anls #关键词提取
class FastJieba(object):

    def __init__(self):
        return
    def cutAll(self,string):
        seg_list=jieba.cut(string, cut_all=True)
        print("【全模式】：" + "/ ".join(seg_list))
    def cut(self,string,HMM=True):
        seg_list=jieba.cut(string, cut_all=False,HMM=HMM)
        print("【精确模式】：" + "/ ".join(seg_list))
    def cut_for_search(self,str):
        seg_list = jieba.lcut_for_search(str)
        print("【搜索模式】：{0}".format(seg_list))
    def load_dict(self):
        jieba.load_userdict("user_dict.txt")

    def pseg_cut(self,str,HMM=True):
        words =pseg.cut(str,HMM=HMM)
        for word, flag in words:
             print("{0} {1}".format(word, flag))
if __name__ == "__main__":
     fj = FastJieba()
     str='外媒：谷歌I/O开发者大会将于5月12日举行'
     fj.cutAll(str)
     fj.cut(str,HMM=False)
     fj.cut(str)
     fj.cut_for_search(str)
     fj.pseg_cut(str)

