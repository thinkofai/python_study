from py2neo import Graph, Node, Relationship, cypher, Path,Subgraph,NodeMatcher
import neo4j
from neo4j.models.NodeModel import NodeModel
class FastNeo4j():
    graph=None

    def connectDB(self):
        self.graph = Graph("http://localhost:7474", username="neo4j", password="123123")

    def getGragh(self):
        if self.graph is None:
            self.connectDB()
        return self.graph

    def matchItembyLabel(self,label,**props):

            sql = "MATCH (n:{} ) return n;".format(label)

            answer = self.getGragh().run(sql,**props).data()
            return answer

    def createNode(self, *label,**props):

        n=Node(*label,**props)
        self.getGragh().create(n)
        return n

    def addNode(self,node:Node):
        self.getGragh().create(node)
    def createRel(self,r:Relationship):
        #Relationship(*start_node *, *type *, *end_node *, ** * properties *)
        self.getGragh().create(r);

        return False
    def createRelByNodes(self,r:Relationship):

        self.getGragh().create(r)

    def addlabel(self,label,node:Node):

        node.add_label(label)
        self.updateByNode(node)


    def delAll(self):

        self.getGragh().delete_all();
    def delByMacth(self,*lable,**props):
        nodes=self.findByProps(*lable,**props)


        sub=Subgraph(nodes=nodes)
        self.getGragh().delete(sub)
    def delNodes(self,*nodes):

        lst=list()
        lst.append(*nodes)
        sub = Subgraph(nodes=lst)
        self.getGragh().delete(sub)


    def updateByNode(self,n:Node):
        lst = list()
        lst.append(n)
        sub = Subgraph(nodes=lst)
        tx = self.getGragh().begin()
        tx.push(sub)
        tx.commit()
    def updateNodeByProps(self,n:Node,**props):
        lst = list()
        for key, value in props.items():
            n[key] = value
        lst.append(n)
        sub = Subgraph(nodes=lst)
        tx = self.getGragh().begin()
        tx.push(sub)
        tx.commit()

    def findOneByProps(self,label,**props):
        matcher = NodeMatcher(self.getGragh())

        return  matcher.match(label,**props).first()

    def findByProps(self, label,  **props):
        matcher = NodeMatcher(self.getGragh())

        return matcher.match(label, **props)

    def findByPropsAndLimit(self, label,limit:int, **props):
        matcher = NodeMatcher(self.getGragh())

        return matcher.match(label, **props).limit(limit)

    def findByCqlsAndLimit(self, label, limit: int, cql:str):
        return  self.getGragh().run(cql).data()


    def findAll(self, label, **props):
        matcher = NodeMatcher(self.getGragh())

        return matcher.match(label, **props)


if __name__ == "__main__":
    n=FastNeo4j()
    # data=n.matchItembyLabel(label='Database')
    # n.createNode(None)
    # a=n.createNode("language","sentence",'template',name='define')
    # b=n.createNode("language","sentence",name='template')
    a=n.findOneByProps("template",name='define')
    b=n.findOneByProps('sentence',name='template')
    # print(a)
    # print(b)
    r=Relationship(a,'ttt2',b)
    n.createRelByNodes(r)
    #n.delAll()
   #  a=n.matchItembyLabel("template",name='define')
   #  b=n.matchItembyLabel("sentence",name='template')
   #  r= Relationship(a,'instanceOf',b)
   #  n.getGragh().create(r)
    # #nns=list()
    # for node in ns:s
    #n.delAll()
    # sub=Subgraph(nodes=nns)
    # n.updateNode(sub)
    #n.delByMacth("temple")
    #b = n.createNode("language", "sentence","temple" ,name='include')
    #print(b)