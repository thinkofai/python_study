from py2neo import Graph, Node, Relationship, cypher, Path,Subgraph,NodeMatcher
import neo4j
from neo4j.models.NodeModel import NodeModel
class Neo4jMethod():
    def __init__(self):
        return

    def connectDB(self):
        self.graph = Graph("http://localhost:7474", username="neo4j", password="123456")

    def getGragh(self):
        if self.graph is None:
            self.connectDB()
        return self.graph

    def shortPath(self,node1:Node,node2:Node,label):
        cql=("MATCH (p1:{} {name:\"" + str(node1['name']) + "\"}),(p2:{}{name:\"" + str(
            node2['name']) + "\"}),p=shortestpath((p1)-[rel:RELATION*]-(p2)) RETURN rel").format(label)
        answer = self.graph.run().evaluate()
        return  answer
        #p = shortestpath((p1) - [*..10]-(p2))


       # 这里[*.        .10]表示路径深度10以内查找所有存在的关系中的最短路径关系